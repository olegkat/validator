// A program to validate a JSON array of objects of the form:
//
//     {
//        "name" :    "Penny Lane",
//        "address" : "123 Main St.",
//        "zip" :     "12345"
//     }
//
// Data file can be specified as an argument on the command line; if
// omitted, standard input will be read.
//
// The program produces six output files with a JSON array of invalid
// objects from the input data.  The files are:
//
//     * bad_names.json:  objects with the "name" field null, missing
//         or blank;
//     * bad_addresses.json:  objects with the "address" field null,
//         missing or blank;
//     * bad_zips.json:  objects with the "zip" field null, missing or
//         an invalid U.S. zipcode;
//     * dups_name_address.json:  objects that are duplicates of
//         another record in both the name and the address fields;
//     * dups_name.json:  objects that are duplicates of another
//         record in the name field only;
//     * dups_address.json:  objects that are duplicates of another
//         record in the address field only.
//
// Objects in the output file will have a new field "offset" added
// with the byte offset of the object in the original data file.
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"strings"
	"unicode"
)

// normalizedString returns the string converted to all lower-case
// runes, with leading and trailing spaces removed, and any runs of
// more than one space collapsed to a single space (' ').
func normalizeString(s *string) string {
	if s == nil {
		return ""
	}

	ss := strings.ToLower(strings.TrimSpace(*s))
	norm := make([]rune, 0, len(ss))

	// Collapse internal runs of space.
	insideSpace := false
	for _, c := range ss {
		if insideSpace {
			if !unicode.IsSpace(c) {
				norm = append(norm, ' ', c)
				insideSpace = false
			}
		} else {
			if unicode.IsSpace(c) {
				insideSpace = true
			} else {
				norm = append(norm, c)
			}
		}
	}

	return string(norm)
}

func openOutFile(name string) *os.File {
	file, err := os.OpenFile(name, os.O_CREATE|os.O_EXCL|os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatal(err)
	}

	return file
}

func closeOutFile(rw *RecordWriter) {
	if err := rw.Finalize(); err != nil {
		log.Fatal(err)
	}

	// Make sure to close a file, to catch any errors during
	// close.
	if f, ok := rw.w.(*os.File); ok {
		if err := f.Close(); err != nil {
			log.Fatal(err)
		}
	}
}

var zipRegexp *regexp.Regexp = func() *regexp.Regexp {
	re, err := regexp.Compile(`^[[:digit:]]{5}$`)
	if err != nil {
		log.Fatal(err)
	}

	return re
}()

func isValidZip(zip *string) bool {
	if zip == nil {
		return false
	}

	return zipRegexp.MatchString(*zip)
}

func isDup(k string, m map[string]int64) bool {
	// Check if the key is empty, or consists of empty components
	// (separated by '\x00').
	empty := true
	for _, c := range k {
		if c != '\x00' {
			empty = false
			break
		}
	}

	if !empty { // empty keys are not dups
		if _, ok := m[k]; ok {
			return true
		}

		m[k] = 1
	}

	return false
}

func main() {
	// Command line.
	progName := path.Base(os.Args[0])
	log.SetPrefix(fmt.Sprintf("%s: ", progName))
	log.SetFlags(0)

	if len(os.Args) > 2 {
		log.Fatalf("Usage: %s [JSON_FILE]\n", progName)
	}

	// Input.
	fileIn := os.Stdin
	if len(os.Args) == 2 {
		var err error

		fileIn, err = os.Open(os.Args[1])
		if err != nil {
			log.Fatal(err)
		}
	}

	// Output.
	wDupsNameAddress := NewRecordWriter(openOutFile("dups_name_address.json"))
	wDupsName := NewRecordWriter(openOutFile("dups_name.json"))
	wDupsAddress := NewRecordWriter(openOutFile("dups_address.json"))
	wNames := NewRecordWriter(openOutFile("bad_names.json"))
	wAddresses := NewRecordWriter(openOutFile("bad_addresses.json"))
	wZips := NewRecordWriter(openOutFile("bad_zips.json"))

	var nameAddressSet = make(map[string]int64)
	var nameSet = make(map[string]int64)
	var addressSet = make(map[string]int64)

	dec := json.NewDecoder(fileIn)

	// Read the first token (delimiter '[') in order to be able to
	// stream-read the objects.
	if t, err := dec.Token(); err != nil {
		log.Fatalf("invalid input at %d: %v\n", dec.InputOffset(), err)
	} else {
		d, ok := t.(json.Delim)
		if !ok || d.String() != "[" {
			log.Fatalf("invalid input at %d: expecting array of objects, found \"%v\"\n", dec.InputOffset(), t)
		}
	}

	// Decode the array of objects and validate the objects.
	for dec.More() {
		r := &Record{}
		offs := dec.InputOffset()

		if err := dec.Decode(r); err != nil {
			log.Fatalf("invalid input at %d: %v\n", dec.InputOffset(), err)
		}

		r.Offset = offs

		name := normalizeString(r.Name)
		address := normalizeString(r.Address)
		nameAddress := name + "\x00" + address // compound key

		if name == "" {
			if err := wNames.WriteRecord(r); err != nil {
				log.Fatal(err)
			}
		}

		if address == "" {
			if err := wAddresses.WriteRecord(r); err != nil {
				log.Fatal(err)
			}
		}

		if !isValidZip(r.Zip) {
			if err := wZips.WriteRecord(r); err != nil {
				log.Fatal(err)
			}
		}

		if isDup(nameAddress, nameAddressSet) {
			if err := wDupsNameAddress.WriteRecord(r); err != nil {
				log.Fatal(err)
			}
		}

		if isDup(name, nameSet) {
			if err := wDupsName.WriteRecord(r); err != nil {
				log.Fatal(err)
			}
		}

		if isDup(address, addressSet) {
			if err := wDupsAddress.WriteRecord(r); err != nil {
				log.Fatal(err)
			}
		}
	}

	// Make sure the input array is terminated correctly (with a "]").
	if t, err := dec.Token(); err != nil {
		log.Fatalf("invalid input at %d: %v\n", dec.InputOffset(), err)
	} else {
		d, ok := t.(json.Delim)
		if !ok || d.String() != "]" {
			log.Fatalf("invalid input at %d: expecting end of array, found \"%v\"\n", dec.InputOffset(), t)
		}
	}

	// Terminate and close all output files.
	closeOutFile(wDupsNameAddress)
	closeOutFile(wDupsName)
	closeOutFile(wDupsAddress)
	closeOutFile(wNames)
	closeOutFile(wAddresses)
	closeOutFile(wZips)
}
