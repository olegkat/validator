package main

import (
	"strings"
	"testing"
)

func TestNormalizeString(t *testing.T) {
	if 0 != strings.Compare(normalizeString(nil), "") {
		t.Error()
	}

	if s := ""; 0 != strings.Compare(normalizeString(&s), "") {
		t.Error()
	}

	if s := "aBc"; 0 != strings.Compare(normalizeString(&s), "abc") {
		t.Error()
	}

	if s := " 	abC"; 0 != strings.Compare(normalizeString(&s), "abc") {
		t.Error()
	}

	if s := "Abc 	"; 0 != strings.Compare(normalizeString(&s), "abc") {
		t.Error()
	}

	if s := "	  abc 	"; 0 != strings.Compare(normalizeString(&s), "abc") {
		t.Error()
	}

	if s := "a	B 	 c"; 0 != strings.Compare(normalizeString(&s), "a b c") {
		t.Error()
	}

	if s := "\n	A	b 	 C  "; 0 != strings.Compare(normalizeString(&s), "a b c") {
		t.Error()
	}
}

func TestIsValidZip(t *testing.T) {
	if isValidZip(nil) {
		t.Error()
	}

	if z := ""; isValidZip(&z) {
		t.Error()
	}

	if z := "1234"; isValidZip(&z) {
		t.Error()
	}

	if z := "123456"; isValidZip(&z) {
		t.Error()
	}

	if z := " 12345"; isValidZip(&z) {
		t.Error()
	}

	if z := "12345 "; isValidZip(&z) {
		t.Error()
	}

	if z := "12a45"; isValidZip(&z) {
		t.Error()
	}

	if z := "12345"; !isValidZip(&z) {
		t.Error()
	}
}

func TestIsDup(t *testing.T) {
	m := make(map[string]int64)

	if isDup("a", m) {
		t.Error()
	}

	if isDup("b", m) {
		t.Error()
	}

	if !isDup("a", m) {
		t.Error()
	}

	// Empty keys are never dups.
	if isDup("", m) {
		t.Error()
	}

	if isDup("", m) {
		t.Error()
	}

	if isDup("\x00", m) {
		t.Error()
	}

	if isDup("\x00", m) {
		t.Error()
	}

	if isDup("\x00\x00", m) {
		t.Error()
	}

	if isDup("\x00\x00", m) {
		t.Error()
	}
}

func TestRecordWriter(t *testing.T) {
	var b strings.Builder
	var n, a, z string

	// No records.
	b.Reset()
	rw := NewRecordWriter(&b)
	rw.Finalize()

	if 0 != strings.Compare(b.String(), "[]") {
		t.Error(b.String())
	}

	// One record.
	b.Reset()
	rw = NewRecordWriter(&b)
	n = "Alice"
	a = "123 Main St."
	z = "12345"
	if err := rw.WriteRecord(&Record{Name: &n, Address: &a, Zip: &z}); err != nil {
		t.Error(err)
	}
	if err := rw.Finalize(); err != nil {
		t.Error(err)
	}

	if 0 != strings.Compare(b.String(), `[{"name":"Alice","address":"123 Main St.","zip":"12345","offset":0}
]`) {
		t.Error(b.String())
	}

	// Two records.
	b.Reset()
	rw = NewRecordWriter(&b)
	n = "Alice"
	a = "123 Main St."
	z = "12345"
	if err := rw.WriteRecord(&Record{Name: &n, Address: &a, Zip: &z}); err != nil {
		t.Error(err)
	}

	n = "Bob"
	a = "2831 Elm St."
	if err := rw.WriteRecord(&Record{Name: &n, Address: &a}); err != nil {
		t.Error(err)
	}

	if err := rw.Finalize(); err != nil {
		t.Error(err)
	}

	if 0 != strings.Compare(b.String(), `[{"name":"Alice","address":"123 Main St.","zip":"12345","offset":0}
,{"name":"Bob","address":"2831 Elm St.","zip":null,"offset":0}
]`) {
		t.Error(b.String())
	}
}
