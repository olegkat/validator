package main

import (
	"encoding/json"
	"io"
)

type Record struct {
	Name    *string `json:"name"`
	Address *string `json:"address"`
	Zip     *string `json:"zip"`
	Offset  int64   `json:"offset"`
}

type RecordWriter struct {
	w   io.Writer
	enc *json.Encoder
}

func NewRecordWriter(w io.Writer) *RecordWriter {
	return &RecordWriter{w: w}
}

// WriteRecord encodes a record as JSON and writes it out.
func (rw *RecordWriter) WriteRecord(r *Record) error {
	if rw.enc == nil { // very first record
		rw.enc = json.NewEncoder(rw.w)
		if _, err := rw.w.Write([]byte{'['}); err != nil {
			return err
		}
	} else {
		if _, err := rw.w.Write([]byte{','}); err != nil {
			return err
		}
	}

	if err := rw.enc.Encode(r); err != nil {
		return err
	}

	return nil
}

// Finalize terminates the streaming encoding.
func (rw *RecordWriter) Finalize() error {
	if rw.enc == nil {
		if _, err := rw.w.Write([]byte("[]")); err != nil {
			return err
		}
	} else {
		if _, err := rw.w.Write([]byte{']'}); err != nil {
			return err
		}
	}

	return nil
}
